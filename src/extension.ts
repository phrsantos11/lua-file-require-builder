// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
var path = require("path");

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  let disposable = vscode.commands.registerCommand("extension.getLuaFileRequire", () => {
    // The code you place here will be executed every time your command is executed
    var editor = vscode.window.activeTextEditor;

    if (!editor) {
      return;
    }

    const res = editor.document.uri;

    var rootFolder = vscode.workspace.getWorkspaceFolder(res);
    if (!rootFolder) {
      return;
    }

    // Get open filename and remove its lua extension
    var currentlyOpenTabfilePath = editor.document.fileName;
    var currentlyOpenTabfileName = path.basename(currentlyOpenTabfilePath);
    var formattedVarName = currentlyOpenTabfileName.replace(/\.lua/g, "");

    // Get root folder path and replace \ with .
    var rootFolderPath = rootFolder.uri.fsPath;
    rootFolderPath = rootFolderPath.replace(/\\/g, ".");

    // Get open file path and replace \ with .
    var openFilePath = editor.document.fileName;
    openFilePath = openFilePath.replace(/\\/g, ".");

    // Create new regex using root folder path to remove it from filepath
    var regex = new RegExp(rootFolderPath);

    var filePathFromRoot = openFilePath.replace(regex, "");
    filePathFromRoot = filePathFromRoot.replace(".src.", "");
    filePathFromRoot = filePathFromRoot.replace(".lua", "");

    // Create formatted require line

    var finalPath = "local " + formattedVarName + ' = require("' + filePathFromRoot + '")';

    // Copy finalPath to clipboard
    vscode.env.clipboard.writeText(finalPath);

    // Show message
    // vscode.window.showInformationMessage("File 'require' copied to clipboard!");
  });
  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
